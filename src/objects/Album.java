package objects;

/**
 * Album
 */
public class Album extends Livres {

   public int[] coloredPage;

 

   public Album(String nom, String auteur, double prix, int pagesCount, int[] coloredPage) {
      super(nom,auteur,prix,pagesCount);
      this.coloredPage = coloredPage;
   }

   public void colorPage(int[] page) {
      //DONE
      this.coloredPage = page;
   }

   public boolean checkIfColored(int page) {
      //TODO check if the page is colored
      for (int i = 0; i < coloredPage.length; i++) {
         if(coloredPage[i] == page){
            return true;
         }
      }
      return false;
   }
}