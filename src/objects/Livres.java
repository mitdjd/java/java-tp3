package objects;

/**
 * Livres
 */
public class Livres extends Oevre {

   
   public int pagesCount;

   public Livres(String nom, String auteur, double prix, int pagesCount) {
      super(nom, auteur, prix);
      this.pagesCount = pagesCount;
   }
}