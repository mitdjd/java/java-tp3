package objects;

/**
 * Comic
 */
public class Comic extends Livres {
   public ComicType type;

                            

   public Comic(String nom, String auteur, double prix, int pagesCount, ComicType type) {
      super(nom,auteur,prix,pagesCount);
      this.type = type;
   }

   public String toString() {
      return nom+":"+auteur+":"+Double.toString(prix)+":"+type.toString();
   }

   public void show() {
      System.out.println(this);
   }
}


