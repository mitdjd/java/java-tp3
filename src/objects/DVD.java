package objects;

/**
 * DVD
 */
public class DVD extends Oevre {

   public String duree;   

   public DVD(String nom, String auteur, double prix, String duree) {
      super(nom, auteur, prix);
      this.duree = duree;
   }
}