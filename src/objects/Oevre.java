package objects;

/**
 * Oevre
 */
public class Oevre {

   public String nom;
   public String auteur;
   public double prix;

   public Oevre(String nom, String auteur, double prix) {
      this.nom = nom;
      this.auteur = auteur;
      this.prix = prix;
      
   }
}